package dawood;

import java.awt.*;
import java.awt.event.*;

public class Spiel 
extends Frame 
{
	public static void main(String [] args)
	{
		Spiel ein = new Spiel();
	}
	
	public Spiel()
	{	
		super("Spiel");
		
		dawood.KeyListener tastatur = new dawood.KeyListener(this);
		addKeyListener(tastatur);
		
		
		
		setSize(500, 500);
		setVisible(true);
		
		 bild = getToolkit().getImage("bild.png");
		 auto = getToolkit().getImage("auto.png");
		 
		 
		 
		MediaTracker tracker = new MediaTracker(this);
		
		tracker.addImage(bild, 0);
		tracker.addImage(auto, 1);
		
		
		try
		{
			tracker.waitForAll();
		}
		catch (InterruptedException ex)
		{
			
		}
		
		repaint();
	
	}
	
	public void paint(Graphics g)
	{
		if (bild != null)
		{
			for(int y = 0; y < 5; y++)
			{
					
				
				for (int x = 0; x < 4; x++)
				{
					if (x == this.autoX && y == this.autoY)
					{
						g.drawImage(auto, x * 50, y * 50, this);
					}
					else
					{
						g.drawImage(bild, x * 50, y * 50, this);
					}		
				}
			}			
		}
	}
	
	public void taste(int code )
	{
		if (code == 1)
		{
			this.autoY = this.autoY -1;
			// this.autoY -= 1;
		}
		else if (code == 2)
		{
			// this.autoX = this.autoX + 1;
			this.autoX += 1;
		}
		else if (code == 3)
		{
			this.autoX = this.autoX -1;
		}
		else if (code == 4)
		{
			this.autoY = this.autoY + 1;
		}
		
		repaint();
	}
	
	protected int autoX = 1;
	protected int autoY = 3;
	
	protected Image bild = null;
	protected Image auto = null;
}