package dawood;

import java.awt.event.*;



public class KeyListener implements java.awt.event.KeyListener
{
	public KeyListener (Spiel parent)
	{
		this.parent = parent;
	}
	
	public void keyPressed(KeyEvent event)
	{
		int keyCode = event.getKeyCode();
		
		if (keyCode == KeyEvent.VK_UP)
		{
			this.parent.taste(1);
		}
		else if (keyCode == KeyEvent.VK_RIGHT)
		{
			this.parent.taste(2);
		}
		else if (keyCode == KeyEvent.VK_LEFT)
		{
			this.parent.taste(3);
		}
		else if (keyCode == KeyEvent.VK_DOWN)
		{
			this.parent.taste(4);
		}
	}
	
	public void keyReleased(KeyEvent taste)
	{
		
	}
	
	public void keyTyped(KeyEvent taste)
	{
		
	}
	
	protected Spiel parent = null;
}