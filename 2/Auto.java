import java.util.ArrayList;

class Auto
{
	Auto(int sitze)
	{
		this.sitze = sitze;
		Auto.letzteId = Auto.letzteId + 1;
		this.id = Auto.letzteId;
		this.soldaten = new ArrayList<Soldat>();
	}
	
	public ArrayList<Soldat> soldaten;
	public int sitze;
	public int id;
	
	public static int letzteId = 7;
}