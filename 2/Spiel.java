
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Spiel
  extends JFrame
  implements ActionListener
{
	public static void main(String[] args)
	{
		Spiel spiel = new Spiel();
		spiel.setLocation(100, 100);
		spiel.show();
	}
	
	public Spiel()
	{
		super("Hello, world!");

		setSize(500, 500);

        this.panelMenue = new JPanel();

        JButton buttonNeu = new JButton("Neu");
		buttonNeu.addActionListener(this);
		this.panelMenue.add(buttonNeu);

		this.buttonSoldat = new JButton("Soldat");
		this.buttonSoldat.addActionListener(this);
		this.buttonSoldat.setVisible(false);
	    panelMenue.add(this.buttonSoldat);

        JButton buttonEinsteigen = new JButton("Einsteigen");
		buttonEinsteigen.addActionListener(this);
		panelMenue.add(buttonEinsteigen);

        JButton buttonInformation = new JButton("Information");
		buttonInformation.addActionListener(this);
		this.panelMenue.add(buttonInformation);

        JButton buttonBeenden = new JButton("Beenden");
		buttonBeenden.addActionListener(this);
		this.panelMenue.add(buttonBeenden);

        getContentPane().add(this.panelMenue);
		
		pack();

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent event)
			{
				closeWindow(event.getWindow());
			}
		});
	}

    public void closeWindow(Window window)
	{
		window.setVisible(false);
		window.dispose();
		System.exit(0);
	}

	public void actionPerformed(ActionEvent event)
	{
		String command = event.getActionCommand();
		
		if (command.equals("Beenden") == true)
		{
			closeWindow(this);
		}
		else if (command.equals("Neu") == true)
		{
			this.buttonSoldat.setVisible(!this.buttonSoldat.isVisible());

			pack();
		}
		else if (command.equals("Soldat") == true)
		{
			Soldat neu = new Soldat();
			armee.add(neu);

			this.buttonSoldat.setVisible(false);
			pack();
		}
		else if (command.equals("Information") == true)
		{
			System.out.println("Soldaten: " + armee.size());
			
			for (int i = 0; i < armee.size(); i++)
			{
				Soldat soldat = armee.get(i);
				
				System.out.println("Soldat #" + soldat.id + ", Leben: " + soldat.leben + "%");
			}
			
			System.out.print("\n");
			System.out.println("Autos: " + autos.size());
			
			for (int i = 0; i < autos.size(); i++)
			{
				Auto auto = autos.get(i);
				
				System.out.println("Auto #" + auto.id + ", Belegung: " + auto.soldaten.size() + "/" + auto.sitze);
			}

			System.out.print("\n");
		}
	}

    public JPanel panelMenue = null;
	public JButton buttonSoldat = null;

	public ArrayList<Soldat> armee = new ArrayList<Soldat>();
	public ArrayList<Auto> autos = new ArrayList<Auto>();
}
