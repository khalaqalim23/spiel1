import java.util.Scanner;
import java.util.ArrayList;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.File;

public class Spiel implements Serializable
{
	public static void main(String[] args) 
	{
		Spiel ein = null;
    File dateiSerialisierung = new File("spiel.save");

    if (dateiSerialisierung.exists() != true)
    {
      ein = new Spiel();
      ein.hauptmenue();

      try
      {
        FileOutputStream fileOutputStream = new FileOutputStream(dateiSerialisierung);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(ein);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        return;
      }
    }
    else
    {
      try
      {
        FileInputStream fileInputStream = new FileInputStream(dateiSerialisierung);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        ein = (Spiel)objectInputStream.readObject();
        ein.hauptmenue();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        return;
      }
    }

    try
    {
      FileOutputStream fileOutputStream = new FileOutputStream(dateiSerialisierung);
      ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
      objectOutputStream.writeObject(ein);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      return;
    }
	}
	
	protected void hauptmenue()
	{
		Scanner scanner = new Scanner(System.in);
		
		while (true)
		{
			System.out.println("[N]eu, [E]insteigen, [A]ussteigen, [I]nformation, [B]eenden");

			char eingabe = scanner.next().charAt(0);
			
			if (eingabe == 'E')
			{
				menueEinsteigen();
			}
			else if (eingabe == 'I')
			{
				druckeInformation();
			}
			else if (eingabe == 'B')
			{
				break;
			}
			else if (eingabe == 'N')
			{
				menueNeu();
			}
			else if (eingabe == 'A')
			{
				menueAussteigen();
			}
			else
			{
				fehler();
			}
		}
	}
	
	protected void menueEinsteigen()
	{
		System.out.println("Welcher Soldat?"); 
	
    Scanner scanner = new Scanner(System.in);

		
		int id = scanner.nextInt();
		Soldat gefundenerSoldat = null;
		
		for (int i = 0; i < armee.size(); i++) 
		{
			Soldat einSoldat = armee.get(i);
			
			if (einSoldat.id == id)
			{
				gefundenerSoldat = einSoldat;
				break;
			}
		}
		
		if (gefundenerSoldat != null)
		{
			System.out.println("Welches Auto?");
			
			int idAuto = scanner.nextInt();
			Auto autoZiel = null;
			
			for (int i = 0; i < autos.size(); i++)
			{
				Auto einAuto = autos.get (i);
				
				if (einAuto.id == idAuto )
				{
					autoZiel = einAuto;
					break;
				}
			}
			
			if (autoZiel != null)
			{
				if (autoZiel.soldaten.size() >= autoZiel.sitze) 
				{
					System.out.println("Auto ist schon voll.");
				}
				else
				{
					autoZiel.soldaten.add(gefundenerSoldat);
          armee.remove(gefundenerSoldat);
				  System.out.println("Soldat #" + id + " steigt ins Auto #" + idAuto + " ein.");
				}
			}
			else
			{
				System.out.println("kein Auto mit ID #" + idAuto);
			}
		}
		else 
		{
			System.out.println("kein Soldat mit Id #" + id);
		}
	}
	
	protected void menueAussteigen()
	{
		System.out.println("Welches Auto?"); 
	
    Scanner scanner = new Scanner(System.in);

		
		int id = scanner.nextInt();
		Auto gefundenesAuto = null;
		
		for (int i = 0; i < autos.size(); i++) 
		{
			Auto einAuto = autos.get(i);
			
			if (einAuto.id == id)
			{
				gefundenesAuto = einAuto;
				break;
			}
		}
		
		if (gefundenesAuto != null)
		{
			if (gefundenesAuto.soldaten.size() == 0)
			{
				System.out.println("keine Soldaten im Auto.");
				return;
			}
			
			System.out.println("Welcher Soldat?");
			
			int idSoldat = scanner.nextInt();
			Soldat soldatZiel = null;
			
			for (int i = 0; i < gefundenesAuto.soldaten.size(); i++)
			{
				Soldat einSoldat = gefundenesAuto.soldaten.get (i);
				
				if (einSoldat.id == idSoldat )
				{
					soldatZiel = einSoldat;
					break;
				}
			}
			
			if (soldatZiel != null)
			{
				armee.add(soldatZiel);
				gefundenesAuto.soldaten.remove(soldatZiel);
        System.out.println("Soldat #" + idSoldat + " steigt aus dem Auto #" + id + " aus.");		
			}
			else
			{
				System.out.println("kein Soldat mit ID #" + idSoldat);
			}
		}
		else 
		{
			System.out.println("kein Auto mit Id #" + id);
		}
		

	}
	
	protected void druckeInformation()
	{
		System.out.println("Anzahl Soldaten: " + armee.size());
				
		for (int i = 0; i < armee.size(); i++)
		{
			Soldat soldat = armee.get(i);
			
			System.out.println("Soldat #" + soldat.id + ", Leben: " + soldat.leben + "%");
		}
		
		System.out.print("\n");
		System.out.println("Anzahl Autos: " + autos.size());
		
		for (int i = 0; i < autos.size(); i++)
		{
			Auto auto = autos.get(i);
			
			System.out.println("Auto #" + auto.id + ", Belegung: " + auto.soldaten.size() + "/" + auto.sitze);
			
			for (int j = 0; j < auto.soldaten.size(); j++)
			{
				Soldat soldat = auto.soldaten.get(j);

				System.out.println(" -> Soldat #" + soldat.id + " sitzt drin, Leben: " + soldat.leben + "%");
			}
		}

		System.out.print("\n");
	}
	
	protected void menueNeu()
	{
    System.out.println("[S]oldat, [A]uto");
		
		Scanner scanner = new Scanner(System.in);
				 
		char eingabe = scanner.next().charAt(0);
		
		if (eingabe == 'S')
		{
			this.letzteIdSoldat += 1;

			Soldat neu = new Soldat(this.letzteIdSoldat);
			armee.add(neu);
		}
		else if (eingabe == 'A')
		{
			System.out.println("Wie viele Sitze?");
			
			int sitze = scanner.nextInt();
			
			this.letzteIdAuto += 1;
			
			Auto neu = new Auto(this.letzteIdAuto, sitze);
			autos.add(neu);
		}
		else 
		{
			fehler();
		}	
	}
	
	protected void fehler()
	{
		System.out.println("FEHLER!");
	}

	protected int letzteIdSoldat = 0;
	protected int letzteIdAuto = 0;

	protected ArrayList<Soldat> armee = new ArrayList<Soldat>();
	protected ArrayList<Auto> autos = new ArrayList<Auto>();
}
