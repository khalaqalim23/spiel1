import java.util.ArrayList;
import java.io.Serializable;

class Auto implements Serializable
{
	Auto(int id, int sitze)
	{
		this.sitze = sitze;
		this.id = id;
		this.soldaten = new ArrayList<Soldat>();
	}
	
	public ArrayList<Soldat> soldaten;
	public int sitze;
	public int id;
}
